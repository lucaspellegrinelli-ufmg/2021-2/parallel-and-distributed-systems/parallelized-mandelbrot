#ifndef MANDELBROT_H
#define MANDELBROT_H

#define MAXITER 32768

#include "types.h"

void fractal(fractal_param_t* p, int color_pick);

#endif