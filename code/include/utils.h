#ifndef UTILS_H
#define UTILS_H

#include "types.h"

#include <utility>
#include <vector>

int get_next_param(FILE * input_file, fractal_param_t * p);

int find_task_to_work(fractal_param_t * task_queue, int n_threads);
int find_open_queue_spot(fractal_param_t * task_queue, int n_threads);
int number_of_items(fractal_param_t * task_queue, int n_threads);
int count_eows(fractal_param_t * task_queue, int n_threads);

std::pair<double, double> calculate_mean_std(std::vector<int> data);
void print_statistics(worker_thread_data_t * worker_data[], master_thread_data_t * master_data);

#endif