#ifndef MASTER_THREAD_H
#define MASTER_THREAD_H

#include "types.h"

void * master(void * data);

void fill_queue(master_thread_data_t * thread_data);
void initialize_workers(master_thread_data_t * thread_data, pthread_t threads[], worker_thread_data_t *worker_data[]);

#endif