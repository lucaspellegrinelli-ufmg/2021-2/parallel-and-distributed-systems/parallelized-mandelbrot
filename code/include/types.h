#ifndef TYPES_H
#define TYPES_H

#include <stdio.h>
#include <vector>
#include <pthread.h>

#define EOW_FRACTAL_PARAM { 0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0, false, true }
#define EMPTY_FRACTAL_PARAM { 0, 0, 0, 0, 0.0, 0.0, 0.0, 0.0, true, false }

typedef struct {
    pthread_mutex_t all_threads_ready_mutex;
    pthread_cond_t all_threads_ready_cond;
    pthread_mutex_t can_read_queue_mutex;
    pthread_cond_t master_filled_queue_cond;
} posix_objects_t;

typedef struct {
	int left;
    int low;
	int ires;
    int jres;
	double xmin;
    double ymin;
	double xmax;
    double ymax;
    bool is_empty;
    bool is_eow;
} fractal_param_t;

typedef struct {
	int thread_id;
    fractal_param_t * task_queue;
    posix_objects_t * posix_objects;
    std::vector<long long> task_times;
    int number_of_threads;
    int n_empty_list;
} worker_thread_data_t;

typedef struct {
    FILE * input_file;
    fractal_param_t * task_queue;
    posix_objects_t * posix_objects;
    int number_of_threads;
    bool reached_eof;
    bool released_threads;
} master_thread_data_t;

#endif