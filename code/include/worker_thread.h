#ifndef WORKER_THREAD_H
#define WORKER_THREAD_H

#include "types.h"

void * worker(void * data);
void do_work(worker_thread_data_t * thread_data, fractal_param_t * p);

#endif