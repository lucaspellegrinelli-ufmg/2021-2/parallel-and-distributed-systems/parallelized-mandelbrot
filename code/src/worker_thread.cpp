#include "worker_thread.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>

#include "utils.h"
#include "mandelbrot.h"

void * worker(void * data) {
    worker_thread_data_t * thread_data = (worker_thread_data_t *) data;

    // Holds thread until all workers are ready
    pthread_mutex_lock(&thread_data->posix_objects->all_threads_ready_mutex);
    pthread_cond_wait(&thread_data->posix_objects->all_threads_ready_cond, &thread_data->posix_objects->all_threads_ready_mutex);
    pthread_mutex_unlock(&thread_data->posix_objects->all_threads_ready_mutex);

    while (true) {
        pthread_mutex_lock(&thread_data->posix_objects->can_read_queue_mutex);
        
        // Finds somewhere to work
        int task_id = find_task_to_work(thread_data->task_queue, thread_data->number_of_threads);

        if (task_id != -1) {
            // If found a task to do, set it to empty so other thread don't try to take it
            thread_data->task_queue[task_id].is_empty = true;
        }

        // If queue is empty, wait for the master thread to fill it
        while (task_id == -1) {
            // Counts that it couldn't find a task
            thread_data->n_empty_list++;
            
            // Waits for refilling
            pthread_cond_wait(&thread_data->posix_objects->master_filled_queue_cond, &thread_data->posix_objects->can_read_queue_mutex);

            // Finds another task after the refill
            task_id = find_task_to_work(thread_data->task_queue, thread_data->number_of_threads);
        }

        pthread_mutex_unlock(&thread_data->posix_objects->can_read_queue_mutex);
        
        // Gets the next task from the queue
        fractal_param_t p = thread_data->task_queue[task_id];
        thread_data->task_queue[task_id].is_empty = true;

        // If it is EOW, exits
        if (p.is_eow) {
            pthread_exit(NULL);
            break;
        }

        // Works and save statistics
        do_work(thread_data, &p);
    }
}

void do_work(worker_thread_data_t * thread_data, fractal_param_t * p) {
    struct timeval start, end;
    gettimeofday(&start, NULL);
    fractal(p, 5);
    gettimeofday(&end, NULL);
    auto delta_time = (end.tv_sec - start.tv_sec) * 1e6 + (end.tv_usec - start.tv_usec);

    // Stores the time taken to run the fractal function
    thread_data->task_times.push_back(delta_time);
}