#include "master_thread.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "worker_thread.h"
#include "utils.h"

void * master(void * data) {
    master_thread_data_t * thread_data = (master_thread_data_t *) data;

    // Workers data/threads
    pthread_t threads[thread_data->number_of_threads];
    worker_thread_data_t *worker_data[thread_data->number_of_threads];

    // Initialize the worker threads
    initialize_workers(thread_data, threads, worker_data);

    // While there's still things to add to the queue...
    while (count_eows(thread_data->task_queue, thread_data->number_of_threads) < thread_data->number_of_threads) {
        // Wait until queue is low on tasks
        while (number_of_items(thread_data->task_queue, thread_data->number_of_threads) > thread_data->number_of_threads) {
            continue;
        }

        pthread_mutex_lock(&thread_data->posix_objects->can_read_queue_mutex);

        // Refill the queue with the next tasks
        fill_queue(thread_data);

        // Signals to all threads that the queue has been refilled
        pthread_cond_broadcast(&thread_data->posix_objects->master_filled_queue_cond);
        pthread_mutex_unlock(&thread_data->posix_objects->can_read_queue_mutex);

        // If first time here, release the threads
        if (!thread_data->released_threads) {
            pthread_cond_broadcast(&thread_data->posix_objects->all_threads_ready_cond);
            thread_data->released_threads = true;
        }
    }

    // Waiting all threads to finish
    for (int i = 0; i < thread_data->number_of_threads; i++) {
        pthread_join(threads[i], NULL);
    }

    // Displaying thread execution stats
    print_statistics(worker_data, thread_data);

    // Exiting thread
    pthread_exit(NULL);
}

void fill_queue(master_thread_data_t * thread_data) {
    // If there's a position open
    while (find_open_queue_spot(thread_data->task_queue, thread_data->number_of_threads) != -1) {
        fractal_param_t p;

        // If there's still lines to parse
        if (!thread_data->reached_eof && get_next_param(thread_data->input_file, &p) != EOF) {

            // Parse line and add to the queue
            p.is_eow = p.is_empty = false;
            int free_spot = find_open_queue_spot(thread_data->task_queue, thread_data->number_of_threads);
            thread_data->task_queue[free_spot] = p;
        } else {

            // Adds as many EOW as possible (less than the number of threads)
            for (int i = 0; i < thread_data->number_of_threads; i++) {
                int free_spot = find_open_queue_spot(thread_data->task_queue, thread_data->number_of_threads);
                if (free_spot != -1) {
                    fractal_param_t p = EOW_FRACTAL_PARAM;
                    thread_data->task_queue[free_spot] = p;
                }
            }

            break;
        }
    }
}

void initialize_workers(master_thread_data_t * thread_data, pthread_t threads[], worker_thread_data_t *worker_data[]) {
    for (int i = 0; i < thread_data->number_of_threads; i++) {
        std::vector<long long> task_times;
        worker_data[i] = new worker_thread_data_t{
            i,
            thread_data->task_queue,
            thread_data->posix_objects,
            task_times,
            thread_data->number_of_threads,
            0
        };

        int rc = pthread_create(&threads[i], NULL, worker, (void*) worker_data[i]);

        if (rc) {
            fprintf(stderr, "Error: Unable to create worker thread %d\n", i);
            exit(-1);
        }
    }
}