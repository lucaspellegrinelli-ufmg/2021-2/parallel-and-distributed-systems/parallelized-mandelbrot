#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string>

#include "types.h"
#include "master_thread.h"
#include "utils.h"

int main(int argc, char* argv[]) {
    // Loads the inputs file
    FILE * input_file;
    if ((input_file = fopen(argv[1], "r")) == NULL) {
		perror("fdopen");
		exit(-1);
	}

    // Loads the number of threads
    int number_of_threads = 1;
    if (argc == 3) {
        number_of_threads = std::stoi(argv[2]);
    }

    // Initialize all POSIX objects such as mutexes, conditionals and barriers
    posix_objects_t * posix_objects = new posix_objects_t;
    pthread_mutex_init(&(posix_objects->all_threads_ready_mutex), NULL);
    pthread_cond_init(&(posix_objects->all_threads_ready_cond), NULL);
    pthread_mutex_init(&(posix_objects->can_read_queue_mutex), NULL);
    pthread_cond_init(&(posix_objects->master_filled_queue_cond), NULL);

    // Initialize the task queue
    fractal_param_t * task_queue = new fractal_param_t[4 * number_of_threads];  
    for (int i = 0; i < 4 * number_of_threads; i++) {
        task_queue[i] = EMPTY_FRACTAL_PARAM;
    }

    // Creates the master thread
    master_thread_data_t *master_thread_data = new master_thread_data_t{
        input_file,
        task_queue,
        posix_objects,
        number_of_threads,
        false
    };

    pthread_t master_thread;
    int rc = pthread_create(&master_thread, NULL, master, (void *) master_thread_data);
    if (rc) {
        fprintf(stderr, "Error: Unable to create master thread\n");
        exit(-1);
    }

    pthread_exit(NULL);
}