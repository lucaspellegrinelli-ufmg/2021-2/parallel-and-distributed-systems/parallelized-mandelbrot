#include "mandelbrot.h"

#include <stdio.h>

#include "types.h"

// Function to draw mandelbrot set
void fractal(fractal_param_t* p, int color_pick) {
	double dx, dy;
	int i, j, k, color;
	double x, y, u, v, u2, v2;

    /*************************************************************
     * a funcao rectangle deve ser removida na versao com pthreads,
     * ja que as bibliotecas sao conflitantes.
     *************************************************************/
	// rectangle(p->left, p->low, p->left+p->ires-1, p->low+p->jres-1);

	dx = (p->xmax - p->xmin) / p->ires;
	dy = (p->ymax - p->ymin) / p->jres;
	
	// scanning every point in that rectangular area.
	// Each point represents a Complex number (x + yi).
	// Iterate that complex number
	for (j = 0; j < p->jres; j++) {
		for (i = 0; i <= p->ires; i++) {
			x = i * dx + p->xmin; // c_real
			u = u2 = 0; // z_real
			y = j * dy + p->ymin; // c_imaginary
			v = v2 = 0; // z_imaginary

			// Calculate whether c(c_real + c_imaginary) belongs
			// to the Mandelbrot set or not and draw a pixel
			// at coordinates (i, j) accordingly
			// If you reach the Maximum number of iterations
			// and If the distance from the origin is
			// greater than 2 exit the loop
			for (k = 0; (k < MAXITER) && ((u2 + v2) < 4); ++k) {
				// Calculate Mandelbrot function
				// z = z*z + c where z is a complex number

				// imag = 2*z_real*z_imaginary + c_imaginary
				v = 2 * u * v + y;
				// real = z_real^2 - z_imaginary^2 + c_real
				u  = u2 - v2 + x;
				u2 = u * u;
				v2 = v * v;
			}
			if (k == MAXITER) {
				// converging areas are black
				color = 0;
			} else {
				// graphics mode has only 16 colors;
				// choose a range and recycle!
				color = (k >> color_pick) % 16;
			}

			// To display the created fractal
			/******************************************************
            * a funcao putpixel deve ser removida na versao com pthreads,
            * ja que as bibliotecas sao conflitantes. Infelizmente,
            * isso significa que sua versao final nao tera uma saida
            * grafica real.
            ******************************************************/
			//putpixel(p->left+i, p->low+j, color);
		}
	}
}