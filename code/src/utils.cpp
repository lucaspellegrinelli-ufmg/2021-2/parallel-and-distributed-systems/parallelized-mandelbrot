#include "utils.h"

#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <math.h>

#include "types.h"

int get_next_param(FILE * input_file, fractal_param_t * p) { 
	int n = fscanf(input_file, "%d %d %d %d", &(p->left), &(p->low), &(p->ires), &(p->jres));
	if (n == EOF) return n;

	if (n != 4) {
		perror("fscanf(left,low,ires,jres)");
		exit(-1);
	}

	n = fscanf(input_file, "%lf %lf %lf %lf", &(p->xmin), &(p->ymin), &(p->xmax), &(p->ymax));

	if (n != 4) {
		perror("scanf(xmin,ymin,xmax,ymax)");
		exit(-1);
	}

	return 8;
}

int find_task_to_work(fractal_param_t * task_queue, int n_threads) {
	for (int i = 0; i < 4 * n_threads; i++) {
		if (!task_queue[i].is_empty && !task_queue[i].is_eow)
			return i;
	}

	for (int i = 0; i < 4 * n_threads; i++) {
		if (!task_queue[i].is_empty && task_queue[i].is_eow)
			return i;
	}

	return -1;
}

int find_open_queue_spot(fractal_param_t * task_queue, int n_threads) {
	for (int i = 0; i < 4 * n_threads; i++) {
		if (task_queue[i].is_empty)
			return i;
	}

	return -1;
}

int number_of_items(fractal_param_t * task_queue, int n_threads) {
	int c = 0;
	for (int i = 0; i < 4 * n_threads; i++) {
		if (!task_queue[i].is_empty)
			c++;
	}
	return c;
}

int count_eows(fractal_param_t * task_queue, int n_threads) {
	int c = 0;
	for (int i = 0; i < 4 * n_threads; i++) {
		if (task_queue[i].is_eow)
			c++;
	}
	return c;
}

std::pair<double, double> calculate_mean_std(std::vector<long long> data) {
	double mean = 0;
	for (auto x : data)
		mean += x;

	mean /= data.size();
	double std = 0;
	for (auto x : data)
		std += pow(x - mean, 2);
	
	std = sqrt(std / (data.size() - 1));
	return { mean, std };
}

void print_statistics(worker_thread_data_t * worker_data[], master_thread_data_t * master_data) {
	int total_tasks = 0;
	
	int total_empty_queue = 0;
	std::vector<long long> tasks_per_thread;
	std::vector<long long> time_per_task;

	for (int i = 0; i < master_data->number_of_threads; i++) {
		total_tasks += worker_data[i]->task_times.size();
		tasks_per_thread.push_back(worker_data[i]->task_times.size());
		total_empty_queue += worker_data[i]->n_empty_list;

		for (long long task_time : worker_data[i]->task_times) {
			time_per_task.push_back(task_time);
		}
	}

	auto task_count_stats = calculate_mean_std(tasks_per_thread);
	auto task_time_stats = calculate_mean_std(time_per_task);

	printf("Tarefas: total = %d;  média por trabalhador = %f(%f)\n", total_tasks, task_count_stats.first, task_count_stats.second);
	printf("Tempo médio por tarefa: %.6f (%.6f) ms\n", task_time_stats.first * 1e-3, task_time_stats.second * 1e-3);
	printf("Fila estava vazia: %d vezes\n", total_empty_queue);
}
